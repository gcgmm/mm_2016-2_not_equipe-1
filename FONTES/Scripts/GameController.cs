﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameController : MonoBehaviour {

    public int points = 1;

    private Image bloodSplatter;

    public bool shouldDisplayBlood = false;

    public GameObject gameOverScreen;

    // Use this for initialization
    void Start () {
        GameObject go = GameObject.FindGameObjectWithTag("BloodSplatter");
        if (!go)
            return;

        bloodSplatter = go.transform.GetComponents<Image>()[0];
        bloodSplatter.enabled = false;
        HideAllHints();
        gameOverScreen = GameObject.FindGameObjectWithTag("Finish");
        gameOverScreen.SetActive(false);
        StartCoroutine(ObserveDamage());
        StartCoroutine(DisplayBadFoodAdvice());
        StartCoroutine(DisplayGoodFoodAdvice());
    }
	
	// Update is called once per frame
	void Update () {
	    if (points == 0)
        {
            GameOver();
        }
	}

    public void DecreaseScore() {
        shouldDisplayBlood = true;
        points--;
    }

    public void IncreaseScore()
    {
        points++;
    }

    public IEnumerator ObserveDamage()
    {
        while (true)
        {
            if (shouldDisplayBlood)
            {
                bloodSplatter.enabled = true;
                yield return new WaitForSeconds(0.5f);
                bloodSplatter.enabled = false;
                this.shouldDisplayBlood = false;
            }
            yield return new WaitForSeconds(0);
        }
        
    }

    public void StopGame() {
        var roads = GameObject.FindGameObjectsWithTag("Road");
        foreach (GameObject road in roads)
        {
            RoadController temp = road.GetComponent<RoadController>();
            if (temp != null)
                temp.speed = 0;
        }

        var spawns = GameObject.FindGameObjectsWithTag("Spawn");
        foreach (GameObject spawn in spawns)
        {
            SpawnController temp = spawn.GetComponent<SpawnController>();
            if (temp != null)
                temp.shouldSpawn = false;
        }
   
        var spawnables = GameObject.FindGameObjectsWithTag("Spawnable");
        foreach (GameObject spawn in spawnables)
        {
            MoveableObjectController temp = spawn.GetComponent<MoveableObjectController>();
            if (temp != null)
                temp.speed = 0;
        }
    
    }

    public void HideAllHints()
    {
        var hints = GameObject.FindGameObjectsWithTag("Advices");
        foreach (GameObject hint in hints)
        {
            Text temp = hint.GetComponent<Text>();
            if (temp != null)
                temp.enabled = false;
        }
    }

    public IEnumerator DisplayBadFoodAdvice()
    {
        while (true)
        {
            if (ShouldDisplayAdvice("bad"))
            {
                var temp = getAdviceByName("BadFoodAdvice").GetComponent<Text>();
                getAdviceByName("GoodFoodAdvice").GetComponent<Text>().enabled = false;
                temp.enabled = true;
                yield return new WaitForSeconds(1.5f);
                temp.enabled = false;
            }
            yield return new WaitForSeconds(0);
        }
        
    }

    public IEnumerator DisplayGoodFoodAdvice()
    {
        while (true)
        {
            if (ShouldDisplayAdvice("good"))
            {
                var temp = getAdviceByName("GoodFoodAdvice").GetComponent<Text>();
                getAdviceByName("BadFoodAdvice").GetComponent<Text>().enabled = false;
                if (!temp)
                {
                    temp.enabled = true;
                    yield return new WaitForSeconds(1.5f);
                    temp.enabled = false;
                }
                yield return new WaitForSeconds(0);
                
            }
            yield return new WaitForSeconds(0);
        }
        
    }

    public bool ShouldDisplayAdvice(string type)
    {
        switch (type)
        {
            case "bad":
                return points == 9 || points < 2;
            case "good":
                return points == 11;
        }
        return false;
    }

    public GameObject getAdviceByName (string name)
    {
        var hints = GameObject.FindGameObjectsWithTag("Advices");
        foreach (GameObject hint in hints)
        {
            if (hint.name == name)
            {
                return hint;
            }
        }
        return null;
    }

    public void GameOver()
    {
        StopGame();
        getAdviceByName("GoodFoodAdvice").GetComponent<Text>().enabled = false;
        getAdviceByName("BadFoodAdvice").GetComponent<Text>().enabled = false;
        gameOverScreen.SetActive(true);
        Invoke("GoToMainMenu", 2);
    }

    public void GoToMainMenu()
    {
        DeleteSceneData();
        SceneManager.LoadScene("MenuGame");
    }

    public void DeleteSceneData()
    {
        foreach (GameObject o in Object.FindObjectsOfType<GameObject>())
        {
            Destroy(o);
        }
    }
}
