﻿using UnityEngine;
using System.Collections;

public class MoveableObjectController : MonoBehaviour {

    public float speed = 0.3f;

    public GameController controller;

    // Use this for initialization
    public void Start() {
        GameObject go = GameObject.FindGameObjectWithTag("CameraRunner");
        controller = go.GetComponent<GameController>();

    }

    // Update is called once per frame
    void Update()
    {

        if (this.transform.position.z - speed < -60)
        {
            Destroy(this.gameObject);
        }
        else
        {
            this.transform.position = new Vector3(transform.position.x, transform.position.y, this.transform.position.z - speed);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        controller.points--;
        Debug.Log(controller.points);
        Destroy(this.gameObject);

    }
}
