﻿using UnityEngine;
using System.Collections;

public class FoodController : MoveableObjectController {
    

    public bool isBad;

    public GameObject sound;
    // Update is called once per frame

    public void Start() {
        sound = GameObject.FindGameObjectWithTag("BiteSound");
        base.Start();
    }
	void Update () {

        if (this.transform.position.z - speed < -60)
        {
            Destroy(this.gameObject);
        }
        else
        {
            this.transform.position = new Vector3(transform.position.x, transform.position.y, this.transform.position.z - speed);
        }
    }

    void OnTriggerEnter(Collider other) {
        sound.GetComponent<AudioSource>().Play();
        if (isBad)
        {
            controller.DecreaseScore();
        }
        else
        {
            controller.IncreaseScore();
        }
        Destroy(this.gameObject);

    }
}
