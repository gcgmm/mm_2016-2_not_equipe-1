﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class RoadController : MonoBehaviour
{
    public float speed = 0.3f;
    

    void Start()
    {

    }



    void Update()
    {
        if (this.transform.position.z - speed < -60)
        {
            this.transform.position = new Vector3(transform.position.x, transform.position.y, 60);
            ChangeHueOfHouses();
        }
        else
        {
            this.transform.position = new Vector3(transform.position.x, transform.position.y, this.transform.position.z - speed);
        }
                   
    }

    void ChangeHueOfHouses()
    {
        List<GameObject> houses = this.GetHouses();
        foreach (GameObject house in houses) 
        {

            ChangeColorOfChildren(house);
        }

    }

    List<GameObject> GetHouses()
    {
        List<GameObject> houses = new List<GameObject>();

        foreach (Transform child in this.transform)
        {
            if (child.tag == "House")
            {
                houses.Add(child.gameObject);
            }
        }

        return houses;
    }

    void ChangeColorOfChildren(GameObject obj)
    {
        foreach (Transform child in obj.transform)
        {
            var renderer = child.GetComponentInChildren<Renderer>();
            renderer.material.color = GetRandomColor();
        }
    }

    public Color GetRandomColor()
    {
        return Random.ColorHSV();
    }
}
