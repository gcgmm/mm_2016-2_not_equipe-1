﻿using UnityEngine;
using System.Collections;

public class SpawnController : MonoBehaviour {

    public int waitTime = 5; 

    public Transform[] spawnPositions;

    public GameObject[] prefabs;

    public bool shouldSpawn = true;

	// Use this for initialization
	void Start () {
        StartCoroutine(GenerateFood());
    }
	
	// Update is called once per frame
	void Update () {
	    
	}

    private IEnumerator GenerateFood()
    {
        while (shouldSpawn)
        {
            yield return new WaitForSeconds(waitTime);
            GameObject prefab = prefabs[Random.Range(0, prefabs.Length)];
            Instantiate(prefab, spawnPositions[Random.Range(0, spawnPositions.Length)].position, prefab.transform.rotation);
        }
    }

}
