﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HumanQuizScript : MonoBehaviour, ActionInteface {
    public string[] Opcoes = { "CardioVasculhar", "Muscular", "Nervoso", "Respiratorio" };
    public int Opcao = 0;
    public int OpcaoCerta = 0;


	// Use this for initialization
	void Start () {
        Camera.main.GetComponent<GestureListener>().action = this;
	    GeraOpcaoCerta();
        
        

	}

   public void GeraOpcaoCerta(){
       OpcaoCerta = Random.Range(0,3);
       Text txtOpcao = GameObject.Find("TextOpcao").GetComponent<Text>();
       txtOpcao.text = string.Format(txtOpcao.text, Opcoes[OpcaoCerta]);
   }

   public void AjustaPosicao()
   {
       if (Opcao > 3)
       {
           Opcao = 0;
       }
       else
       {
           if (Opcao < 0)
           {
               Opcao = 3;
           }
       }
   }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void RoadToNext()
    {
        Opcao++;
        AjustaPosicao();
    }

    public void RoadToPrevious()
    {
        Opcao--;
        AjustaPosicao();
    }

    public void Jump()
    {
        if (Opcao == OpcaoCerta)
        {
            GameObject.Find("Theme").GetComponent<AudioSource>().Stop();
            AudioSource audio = GameObject.Find("AcertOption").GetComponent<AudioSource>();
            audio.Play();
            
            
        
            StartCoroutine(DelaySceneLoad());            
        }
        else
        {
            GameObject.Find("ErrorOption").GetComponent<AudioSource>().Play();           
        }
    }

    IEnumerator DelaySceneLoad()
    {
        yield return new WaitForSeconds(3.0f);
        GameObject.Find("TextCarregando").GetComponent<Text>().text = "Carregando...";
        KinectManager.DestroyImmediate(KinectManager.Instance);
        SceneManager.LoadScene("RunnerBack");
    }


}
