﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour {

    private GameController controller;

    public Text score;
	// Use this for initialization
	void Start () {
        score = GetComponent<Text>();
        GameObject go = GameObject.FindGameObjectWithTag("CameraRunner");
        controller = go.GetComponent<GameController>();
        Debug.Log(score.text);
    }
	
	// Update is called once per frame
	void Update () {
        score.text = "Pontos: " + controller.points;
	}
}
