﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class QuizController : MonoBehaviour {
    private Dictionary<string, Sprite> Imagens = new Dictionary<string, Sprite>();
    private string[] opcoesButoes = new string[3];


    private string opcaoCerta;

    public Image[] Opcoes;    

	
	void Start () {
        CarregaArquivos();
        DeterminaSistema();
        AjustaTextos();
        RenderImagens();
	}

    public void AjustaTextos()
    {
        Text txtOpcao = GameObject.Find("TextOpcao").GetComponent<Text>();
        txtOpcao.text = string.Format(txtOpcao.text, opcaoCerta);
    }

    public void DeterminaSistema()
    {
        opcaoCerta = getKey(Imagens,Random.Range(0, 3));
    }


    public void CarregaArquivos()
    {
        Imagens.Add("Cardiovasculhar", Resources.Load<Sprite>("Textures/Cardiovasculhar") as Sprite);
        Imagens.Add("Muscular", Resources.Load<Sprite>("Textures/Muscular") as Sprite);
        Imagens.Add("Nervoso", Resources.Load<Sprite>("Textures/Nervoso") as Sprite);
        Imagens.Add("Respiratorio", Resources.Load<Sprite>("Textures/Respiratorio") as Sprite);                
    }
	
	// Update is called once per frame
	void Update () {
	
	
    }

    public void RenderImagens()
    {
       int localCerto = Random.Range(0, 3);
       opcoesButoes[localCerto] = opcaoCerta;
       Opcoes[localCerto].sprite = Imagens[opcaoCerta];

       List<string> opcoes = getOpcoes(opcaoCerta);

       for (int i = 0; i < 3; i++)
       {
           if (i != localCerto)
           {
               opcoesButoes[i] = opcoes[0];
               Opcoes[i].sprite = Imagens[opcoes[0]];
               opcoes.RemoveAt(0);
           }
      }

        
    }

    public List<string> getOpcoes(string opcaoCerta)
    {
        int random = 0;
        List<string> listaOpcoes = new List<string>();
        for (int i = 0; i < 4; i++)
        {
            if (getKey(Imagens,i) != opcaoCerta)
            {
                listaOpcoes.Add(getKey(Imagens,i));
            }
        }

        List<string> combinacao = new List<string>();

        random = Random.Range(0, 2);
        combinacao.Add(listaOpcoes[random]);
        listaOpcoes.RemoveAt(random);

        random = Random.Range(0, 1);
        combinacao.Add(listaOpcoes[random]);

        return combinacao;


    }


    public string getKey(Dictionary<string, Sprite> dictionary, int position)
    {
        int i = 0;       
        foreach (var item in dictionary)
        {
            if (i == position)
            {
                return item.Key;
            }
            i++;
        }
        return "";
    }

    public void OnClick()
    {
        Debug.Log("Passou aqui");
    }

   
}

